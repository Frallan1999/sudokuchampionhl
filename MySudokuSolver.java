package sudoku;

public class MySudokuSolver implements SudokuSolver {
	private int [][] board; 
	//private boolean [][] isFilled; 
	
	
	public MySudokuSolver() {
		board = new int[9][9]; 
	}

	@Override
	public boolean solve() {
		return solve(0, 0);
	}
	
	private boolean solve (int row, int col) {
		
		if (col == board.length) {
			col = 0; 
			row ++; 
		}
		
		if (row == board.length) {
			return true; 
		}
		
		if (get(row, col) != 0) {
			if (isValid(row, col, get(row, col))) {
				return solve(row, col + 1); 
			}
		} else {
			for (int i = 1; i < 10; i ++) {
				if (isValid(row, col, i)) {
					add(row, col, i); 
					
					if (solve(row, col+1)) {
						return true;
					}
					remove(row, col);
					
				}
			}
		}
		return false; 
	}

	@Override
	public void add(int row, int col, int digit) {
		if ( digit < 1 || digit > 9) {
			throw new IllegalArgumentException("Only numbers 1 to 9 are allowed in sudoku"); 
		} 
		if ( row < 0 || row > 8 || col < 0 || col > 8) {
			throw new IllegalArgumentException("The board only has columns 1 to 9 and rows 1 to 9"); 
		}
		
		board[row][col] = digit;
	}

	@Override
	public void remove(int row, int col) {
		if ( row < 0 || row > 8 || col < 0 || col > 8) {
			throw new IllegalArgumentException("The board only has columns 1 to 9 and rows 1 to 9"); 
		}
		
		board[row][col] = 0;
	}

	
	@Override
	public int get(int row, int col) {
		if ( row < 0 || row > 8 || col < 0 || col > 8) {
			throw new IllegalArgumentException("The board only has columns 1 to 9 and rows 1 to 9"); 
		}
		
		return board[row][col];
	}

	@Override
	public boolean isValid() {
		for (int row = 0; row < board.length; row ++) {
			for (int col = 0; col < board.length ; col ++) {
				if (board[row][col] != 0) {
					if ( !isValid(row, col, board[row][col]) ) {
						return false; 
					}
				}
			}
		}
		return true; 
	}
	
	private boolean isValid(int r, int c, int digit) {
		if ( digit < 0 || digit > 9) {
			throw new IllegalArgumentException("Only numbers 1 to 9 are allowed in sudoku"); 
		} 
		if ( r < 0 || r > 8 || c < 0 || c > 8) {
			throw new IllegalArgumentException("The board only has columns 1 to 9 and rows 1 to 9"); 
		}
		
		
		// Case 1: Same number in same row

		for (int i = 0; i < board.length; i++) {
			if (board[i][c] == digit && i != r) {
				return false;  
			}
		}
		
		// Case 2: Same number in same column

		for (int i = 0; i < board.length; i++) {
			if (board[r][i] == digit && i != c) {
				return false; 
			}
		}
		
		// Case 3: Same number in same box
		int startRow = 3; 
		int startCol = 3; 
		
		
			//Calculates which box we are in
		if (r < 3) {
			startRow = 0; 
		}
		
		if (r > 5) {
			startRow = 6; 
		}
		
		if (c < 3) {
			startCol = 0; 
		}
		
		if (c > 5) {
			startCol = 6; 
		}
		
			//Iteration in box
		for (int i = startRow; i < startRow + 3; i++) {
			for (int k = startCol; k < startCol + 3; k++) {
				if (board[i][k] == digit && i != r && k != c) {
					return false; 
				}
			}
			
		}
		
		//Return true if it passes all of the tests
		return true; 
	}

	@Override
	public void clear() {
		board = new int[9][9]; 

	}

	@Override
	public void setMatrix(int[][] m) {
		if (m.length != board.length || m[0].length != board[0].length) {
			throw new IllegalArgumentException("The board only has columns 1 to 9 and rows 1 to 9"); 
		}
		
		for (int i = 0; i < m.length; i++) {
			for ( int k = 0; k < m[0].length; k++) {
				if ( m[i][k] < 0 || m[i][k] > 9 ) {
					throw new IllegalArgumentException("Only numbers 1 to 9 are allowed in sudoku");  
				}
				
				if (m[i][k] != 0) {
					board[i][k] = m[i][k]; 
				}
				
			}
		}
	}

	@Override
	public int[][] getMatrix() {
		return board;
	}

}
