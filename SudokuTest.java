package sudoku;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SudokuTest {
	
	private SudokuSolver sudoku; 

	@BeforeEach
	void setUp() throws Exception {
		sudoku = new MySudokuSolver(); 
	}

	@AfterEach
	void tearDown() throws Exception {
		sudoku = null;  
	}

	@Test
	void testSolveEmpty() {
		assertTrue(sudoku.solve()); 
	}
	
	@Test
	void testSudokuFigOne() {
		int[][] s = {
				{0, 0, 8, 0, 0, 9, 0, 6, 2}, 
				{0, 0, 0, 0, 0, 0, 0, 0, 5},
				{1, 0, 2, 5, 0, 0, 0, 0, 0},
				{0, 0, 0, 2, 1, 0, 0, 9, 0},
				{0, 5, 0, 0, 0, 0, 6, 0, 0},
				{6, 0, 0, 0, 0, 0, 0, 2, 8},
				{4, 1, 0, 6, 0, 8, 0, 0, 0},
				{8, 6, 0, 0, 3, 0, 1, 0, 0},
				{0, 0, 0, 0, 0, 0, 4, 0, 0}};
		
		sudoku.setMatrix(s);
		assertTrue(sudoku.solve());
	}
	
	@Test
	void testWrongInput() {
		sudoku.add(0, 0, 9);
		sudoku.add(1, 0, 9);
		assertFalse(sudoku.solve(), "Can solve even if wrong input"); 
		
	}
	
	@Test
	void testUnsolveble() {
		int[][] s = {
			{1, 2, 3, 0, 0, 0, 0, 0, 0}, 
			{4, 5, 6, 0, 0, 0, 0, 0, 0},
			{7, 8, 9, 7, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0}};
			sudoku.setMatrix(s);
			assertFalse(sudoku.solve());
	}
	
	@Test 
	void testClear() {
		sudoku.add(1, 3, 1);
		sudoku.clear();
		assertEquals(0, sudoku.get(1, 3));
	}
	
	@Test 
	void testGetMatrix() {
		sudoku.add(1, 2, 1);
		int [][] temp = sudoku.getMatrix();
		assertEquals(1, temp [1][2], "The method getMatrix does not work" );
	}
	
	@Test
	void testIsValid () {
		sudoku.add(1, 1, 2);
		sudoku.add(2, 2, 1);
		sudoku.add(3, 2, 1);
		assertFalse(sudoku.isValid());
		
		sudoku.remove(3, 2);
		sudoku.add(3, 3, 3);
		assertTrue(sudoku.isValid());
		
	}
	
	@Test
	void testGet() {
		sudoku.add(1, 1, 2);
		assertEquals(2, sudoku.get(1, 1)); 
	}
	
}
