package sudoku;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

public class SudokuController {
	private MySudokuSolver solver; 
	private JTextField[][] textFields; 
	private JPanel centerPanel; 
	
		public SudokuController(MySudokuSolver solver) {
			this.solver = solver; 
			textFields = new JTextField[9][9];
            SwingUtilities.invokeLater(() -> createWindow(solver, "SudokuSolver", 100, 300));
		}
		
		private void createWindow(MySudokuSolver sudoku, String title , int width, int height) {
			JFrame frame = new JFrame(title);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			Container pane = frame.getContentPane(); // pane är en behållarkomponent till vilken de övriga komponenterna (listvy, knappar etc.) ska läggas till.
		
			//Create Sudoku part
			centerPanel = new JPanel(); 
			centerPanel.setLayout(new GridLayout(9,9));
			centerPanel.setBackground(Color.LIGHT_GRAY);

			
			buildSudokuBoard(); 
			
			pane.add(centerPanel); 
			centerPanel.setBackground(Color.LIGHT_GRAY);

			
			//Create lower part with buttoms 
			JPanel southPanel = new JPanel(); 
			southPanel.setBackground(Color.LIGHT_GRAY);
			pane.add(southPanel, BorderLayout.SOUTH);	
			
			

			
			//SOLVE BUTTON
			JButton solve = new JButton("Solve"); 			 
			southPanel.add(solve);


			solve.addActionListener(event -> {
				if (inputVerification()) {
					if (solver.solve()) {
						rebuildSudokuBoard(); 
						JOptionPane.showMessageDialog(pane, "The sudoku has been solved");
					} else {
						JOptionPane.showMessageDialog(pane, "There is no solution to the sudoku");
					}
				} else {
					JOptionPane.showMessageDialog(pane, "The input is wrong");
				}});
			
			
			//CLEAR BUTTON
			JButton clear = new JButton("Clear");			//CLEAR BUTTON
			southPanel.add(clear);
			
			clear.addActionListener(event -> {
				solver.clear();
				
				for (int row = 0; row < 9; row++) {
					for (int col = 0; col < 9; col++) {
						textFields[row][col].setText(null);
					}
				}
			});
			
			
			frame.pack();
			frame.setVisible(true); 
			
		}
		private void buildSudokuBoard() {
			for (int row = 0; row < 9; row ++) {
				for (int col = 0; col < 9; col++ ) {
					textFields[row][col] = new JTextField(); 
					textFields[row][col].setHorizontalAlignment(JTextField.CENTER);
			
					
					setColour(row, col); 
					centerPanel.add(textFields[row][col]); 
					
				}
			}
		}
		
		
		private boolean inputVerification() {
			for (int row = 0; row < 9; row ++) {
				for (int col = 0; col < 9; col++ ) {
					
					
					String s = textFields[row][col].getText();
					
					if (!s.isBlank()) {
						
						try {
							int digit = Integer.parseInt(s);
							solver.add(row, col, digit);
						} catch (Exception e){
							return false; 
						}
						
					} else {
						solver.remove(row, col);
					}
				}
			}
			return true; 
		}
		
		private void rebuildSudokuBoard() {
			for (int row = 0; row < 9; row++) {
				for (int col = 0; col < 9; col++) {
					String digit = String.valueOf(solver.get(row, col));
					textFields[row][col].setText(digit);
				}
			}
		}
		
		private void setColour(int row, int col) {
			if ((row < 3 && col < 3) || (row < 3 && col > 5)) { // top left and right box
				textFields[row][col].setBackground(Color.pink);
			} else if((row < 6 && row >=3) && (col < 6 && col >= 3)) { //middle box
				textFields[row][col].setBackground(Color.pink);
			} else if (row > 5 && (col < 3 || col > 5)) {
				textFields[row][col].setBackground(Color.pink); //bottom left and right box
			} else {
				textFields[row][col].setBackground(Color.white); //All other boxes

			}
		}
		
		
		
}

